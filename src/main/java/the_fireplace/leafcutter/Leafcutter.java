package the_fireplace.leafcutter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLeaves;
import net.minecraft.block.BlockWeb;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientConnectedToServerEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import the_fireplace.fulcrum.api.API;
import the_fireplace.fulcrum.math.VersionMath;
/**
 * @author The_Fireplace
 */
@Mod(modid=Leafcutter.MODID, name=Leafcutter.MODNAME, version=Leafcutter.VERSION)
public class Leafcutter {
	@Instance(Leafcutter.MODID)
	public static Leafcutter instance;
	public static final String MODID = "leafcutter";
	public static final String MODNAME = "Leafcutter";
	public static final String VERSION = "2.0.3.0";
	static final String downloadURL = "http://goo.gl/x2cH14";

	public static Item leafcutter = new ItemLeafcutter();

	@EventHandler
	public void PreInit(FMLPreInitializationEvent event){
		IC ic;
		if(Loader.isModLoaded("fulcrum")){
			ic=new C();
			ic.r();
		}
		GameRegistry.registerItem(leafcutter, "leafcutter");
	}

	@EventHandler
	public void Init(FMLInitializationEvent event){
		if(event.getSide().isClient()){
			Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(leafcutter, 0, new ModelResourceLocation("leafcutter:leafcutter", "inventory"));
		}
		GameRegistry.addRecipe(new ItemStack(leafcutter), " s ", "t t", "t t",
				's', Items.shears, 't', Items.stick);
	}
}
