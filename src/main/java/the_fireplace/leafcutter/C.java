package the_fireplace.leafcutter;

import the_fireplace.fulcrum.api.API;
import the_fireplace.fulcrum.math.VersionMath;

public class C implements IC {

	@Override
	public void r() {
		API.registerModToVersionChecker(Leafcutter.MODNAME, Leafcutter.VERSION, VersionMath.getVersionFor("https://dl.dropboxusercontent.com/s/s0yvi2k3jmb6jmm/prerelease.version?dl=0"), VersionMath.getVersionFor("https://dl.dropboxusercontent.com/s/jls8z7oqopq95q5/release.version?dl=0"), Leafcutter.downloadURL, Leafcutter.MODID);
	}

}
